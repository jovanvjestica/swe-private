<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//getovi
Route::get('hotel','HotelController@index');
Route::get('client','ClientController@index');
Route::get('employee','EmployeeController@index');
Route::get('event','EventController@index');
Route::get('food','FoodController@index');
Route::get('reservation','ReservationController@index');
Route::get('room','RoomController@index');
Route::get('service','ServiceController@index');
Route::get('review','ReviewController@index');
Route::get('room/{id}/reservations','RoomController@test');
Route::get('client/{id}/events','ClientController@getEvents');
Route::get('uses','UsesController@index');
Route::get('attends','AttendsController@index');

Route::get('current','ClientController@current');
Route::post('room/findRooms','RoomController@findRooms');

//getovi za jedne

Route::get('hotel/{id}','HotelController@index');
Route::get('client/{id}','ClientController@index');
Route::get('employee/{id}','EmployeeController@index');
Route::get('event/{id}','EventController@index');
Route::get('food/{id}','FoodController@index');
Route::get('reservation/{id}','ReservationController@index');
Route::get('room/{id}','RoomController@index');
Route::get('service/{id}','ServiceController@index');
Route::get('review/{id}','ReviewController@index');


//postovi
Route::post('hotel','HotelController@store');
Route::post('client','ClientController@store');
Route::post('employee','EmployeeController@store');
Route::post('event','EventController@store');
Route::post('food','FoodController@store');
Route::post('reservation','ReservationController@store');
Route::post('room','RoomController@store');
Route::post('service','ServiceController@store');
Route::post('review','ReviewController@store');
Route::post('uses','UsesController@store');
Route::post('attends','AttendsController@store');

//putovi
Route::put('hotel/{id}','HotelController@update');
Route::put('client/{id}','ClientController@update');
Route::put('employee/{id}','EmployeeController@update');
Route::put('event/{id}','EventController@update');
Route::put('food/{id}','FoodController@update');
Route::put('reservation/{id}','ReservationController@update');
Route::put('room/{id}','RoomController@update');
Route::put('service/{id}','ServiceController@update');
Route::put('review/{id}','ReviewController@update');
Route::put('uses/{id}','UsesController@update');
Route::put('attends/{id}','AttendsController@update');


//deletovi
Route::delete('hotel/{id}','HotelController@destroy');
Route::delete('client/{id}','ClientController@destroy');
Route::delete('employee/{id}','EmployeeController@destroy');
Route::delete('event/{id}','EventController@destroy');
Route::delete('food/{id}','FoodController@destroy');
Route::delete('reservation/{id}','ReservationController@destroy');
Route::delete('room/{id}','RoomController@destroy');
Route::delete('service/{id}','ServiceController@destroy');
Route::delete('review/{id}','ReviewController@destroy');
Route::delete('uses/{id}','UsesController@destroy');
Route::delete('attends/{id}','AttendsController@destroy');

Route::post('login','PassportController@login');
Route::post('register','PassportController@register');
Route::post('logout','PassportController@logout');
Route::post('get-details','PassportController@getDetails');

Route::post('loginEmployee','PassportController@loginEmployee');
Route::post('registerEmployee','PassportController@registerEmployee');
Route::post('logoutEmployee','PassportController@logoutEmployee');
Route::post('get-detailsEmployee','PassportController@getDetailsEmployee');