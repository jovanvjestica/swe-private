<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

use App\Employee as Employee;

class Employee extends Authenticatable
{
    use Notifiable,HasApiTokens;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "employee";
    protected $guard = "radnik";
    protected $primaryKey= "ID";
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    protected $guarded = ['ID'];
    protected $hidden = ['PASSWORD'];

    public function hotel()
    {
        return $this->belongsTo('App\Hotel','ID_HOTEL','ID');
    }

    public function getAuthPassword()
    {
        return $this->PASSWORD;
    }
}
