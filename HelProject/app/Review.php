<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "review";
    protected $guarded = ['ID'];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $primaryKey= "ID";

    public function client()
    {
        return $this->hasOne('App\Client','ID','ID_CLIENT');
    }
}
