<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attends extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "attends";
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $guarded = ['ID'];
    protected $primaryKey= "ID";

    
}
