<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uses extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "uses";
    protected $guarded = ['ID'];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $primaryKey= "ID";
}
