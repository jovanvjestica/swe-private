<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "service";
    protected $primaryKey= "ID";
    protected $guarded = ['ID'];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function hotel()
    {
        return $this->belongsTo('App\Hotel','ID_HOTEL','ID');
    }

    public function foods()
    {
        return $this->hasMany('App\Food','SERVICE_ID','ID');
    }
}
