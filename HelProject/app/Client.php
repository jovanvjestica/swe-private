<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

use App\Client as Client;

class Client extends Authenticatable
{
    use Notifiable,HasApiTokens;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "client";
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    //protected $fillable = array('*');

    protected $guarded = ['ID'];

    protected $hidden = [
        'PASSWORD','EMAIL'
    ];

    public function getAuthPassword()
    {
        return $this->PASSWORD;
    }

    protected $primaryKey= "ID";

    public function reservation()
    {
        return $this->hasOne('App\Reservation','ID_RESERVATION','ID');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event','attends','ID_CLIENT','ID_EVENT');
    }

    public function review()
    {
        return $this->belongsTo('App\Review','ID','ID_CLIENT');
    }
}
