<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "food";
    protected $primaryKey= "ID";
    protected $guarded = ['ID'];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function service()
    {
        return $this->belongsTo('App\Service', 'SERVICE_ID','ID');
    }
}
