<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\TokenClient as TokenClient;
use App\TokenEmployee as TokenEmployee;
use App\Employee as Employee;
use App\Client as Client;
use Cookie;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function lookUp($type, $level)
    {
        //$ctype = Cookie::get('type');
        //$ctoken = Cookie::get('token');
        $ctype= request()->type;
        $ctoken = request()->token;
        if(strcmp($ctype,$type)!=0)
            return false;
        $user;
        if(strcmp($ctype,'employee')==0)
        {
            $te = TokenEmployee::where('TOKEN',$ctoken)->where('FINISH',TokenEmployee::max('FINISH'))->first();
            if($te==null || strtotime($te->FINISH)<time())
                return false;
            $user = Employee::find($te->ID_EMPLOYEE);
            $i;
            $k;
            if(strcmp($user->TYPE,'admin')==0)
                $i=3;
            if(strcmp($user->TYPE,'manager')==0)
                $i=2;
            if(strcmp($user->TYPE,'regular')==0)
                $i=1;
            if(strcmp($level,'admin')==0)
                $k=3;
            if(strcmp($level,'manager')==0)
                $k=2;
            if(strcmp($level,'regular')==0)
                $k=1;
            return ($i>=$k);
        }
        else if(strcmp($ctype,'client')==0)
        {
            $tc = TokenClient::where('TOKEN',$ctoken)->where('FINISH',TokenClient::max('FINISH'))->first();
            if($tc==null || strtotime($tc->FINISH)<time())
                return false;
            $user = Client::find($te->ID_CLIENT);
            $i;
            $j;
            if(strcmp($level,'registered')==0)
                $j=2;
            else 
                $j=1;
            $i = $user->REGISTERED+1;
            return $i>=$j;
        }
        return false;
    }

    public function getUser()
    {
       // $ctype = Cookie::get('type');
       // $ctoken = Cookie::get('token');
        $ctype = request()->type;
        $ctoken = request()->token;
        if(strcmp($ctype,'client')==0)
        {
            $tc = TokenClient::where('TOKEN',$ctoken)->where('FINISH',TokenClient::max('FINISH'))->first();
            if($tc==null || strtotime($tc->FINISH)<time())
                return null;
            $user = Client::find($te->ID_CLIENT);
            return $user;
        }
        else if(strcmp($ctype,'employee')==0)
        {
            $te = TokenEmployee::where('TOKEN',$ctoken)->where('FINISH',TokenEmployee::max('FINISH'))->first();
            if($te==null || strtotime($te->FINISH)<time())
                return null;
            $user = Employee::find($te->ID_EMPLOYEE);
            return $user;
        }
        return null;
    }
}
