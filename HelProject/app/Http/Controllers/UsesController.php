<?php

namespace App\Http\Controllers;
use App\Uses as Uses;
use Illuminate\Http\Request;

class UsesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Uses::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $u = new Uses;
        $u->ID_CLIENT=$request['ID_CLIENT'];
        $u->ID_SERVICE=$request['ID_SERVICE'];
        $u->DURATION =$request['DURATION'];
        $u->START_TIME=$request['START_TIME'];
        $u->END_TIME=$request['END_TIME'];
        $u->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $u = Uses::find($id);
        $u->ID_CLIENT=$request['ID_CLIENT'];
        $u->ID_SERVICE=$request['ID_SERVICE'];
        $u->DURATION =$request['DURATION'];
        $u->START_TIME=$request['START_TIME'];
        $u->END_TIME=$request['END_TIME'];
        $u->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Uses::destroy($id);
    }
}
