<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee as Employee;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->lookUp('employee','manager'))
            return Employee::all();
        else
            return response()->json(['failiure'=>'Not authorised!'],401);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$this->lookUp('employee','manager'))
            return response()->json(['failiure'=>'Access denied'],401);
        /*$e = new Employee;
        $e->NAME = $request->NAME;
        $e->LAST_NAME = $request->LAST_NAME;
        $e->USERNAME = $request->USERNAME;
        $e->PASSWORD = $request->PASSWORD;
        $e->JMBG = $request->JMBG;
        $e->SALARY = $request->SALARY;
        $e->TYPE = $request->TYPE;
        $e->BONUS = $request->BONUS;
        $e->ID_HOTEL = $request->ID_HOTEL;
        $e->save();*/
        Employee::create($request->all());
        return response()->json(['success'=>'Added new employee!'],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$this->lookUp('employee','regular'))
            return response()->json(['failiure'=>'Access denied'],401);
        else
        {
            $user = $this->getUser();
            $e=null;
            if($this->lookUp('employee','manager') || $user->ID == $id)
            {
                $e = Employee::find($id);
                $e->NAME = $request->NAME;
                $e->LAST_NAME = $request->LAST_NAME;
                $e->USERNAME = $request->USERNAME;
                $e->PASSWORD = $request->PASSWORD;
            }
            if($this->lookUp('employee','manager'))
            {
                $e->JMBG = $request->JMBG;
                $e->SALARY = $request->SALARY;
                $e->TYPE = $request->TYPE;
                $e->BONUS = $request->BONUS;
                $e->ID_HOTEL = $request->ID_HOTEL;
            }
            if($e==null)
                return response()->json(['failiure'=>'Not authorized!'],401);    
            $e->save();
            return response()->json(['success'=>'You have edited an employees data!'],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$this->lookUp('employee','manager'))
            return response()->json(['failiure'=>'Not authorized!'],401);    
        Employee::destroy($id);
        return response()->json(['success'=>'Youre fired!'],200);
    }

    public function getHotel($id)
    {
        return Employee::find($id)->hotel;
    }
}
