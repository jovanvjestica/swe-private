<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Hash as Hash;
use Illuminate\Support\Facades\Response;
use App\TokenClient as TokenClient;
use App\TokenEmployee as TokenEmployee;
use Cookie;

use App\Http\Controllers\Controller;

use App\Client as Client;
use App\Employee as Employee;

class PassportController extends Controller
{
    public $successStatus=200;

    public function username()
    {
        return 'USERNAME';
    }

    public function login()
    {
        $request['USERNAME']=request('USERNAME');
        $request['PASSWORD']=stripslashes(request('PASSWORD'));
        $u = Client::where('USERNAME',$request['USERNAME'])->first();
        if($u==null)
            return response()->json(['error'=>'Unauthorised: username does not exist'],401);
        $password = $u['PASSWORD'];
        if(Hash::check($request['PASSWORD'],$password))
        {
            $user = Client::where('USERNAME',$request['USERNAME'])->where('PASSWORD',$password)->first();
            $token = TokenClient::where('ID_CLIENT',$user->ID)->where('FINISH',TokenClient::where('ID_CLIENT',$user->ID)->max('FINISH'))->first();
            $timestamp = strtotime($token->FINISH);
            if($token==null || $timestamp<time())
            {
                $token = new TokenClient;
                $token->TOKEN=Hash::make(rand());
                $token->ID_CLIENT = $user->ID;
                $token->PRIVILEDGE = 0;
                $token->START = date('Y-m-d h:i:s', time());
                $token->FINISH=date('Y-m-d h:i:s', time()+60*60*8);
                $token->save();
            }
            else
            {
                $token->FINISH = date('Y-m-d h:i:s', time()+60*60*8);
                $token->save();
            }
            return response()->json(['success'=>'You have been logged in.','token'=>$token->TOKEN],200)->withCookie(cookie('token',$token->TOKEN),60)->withCookie(cookie('type','client',60))->header('Access-Control-Allow-Credentials','true');
        }
        else
            return response()->json(['error'=>'Unauthorised: wrong password'],401);
   }

   public function logout(Request $request)
   {
        $c1 = Cookie::forget('token');
        $c2 = Cookie::forget('type');
        return response()->json(['success'=>'You have been logged out'],200)->withCookie($c1)->withCookie($c2);
   }

   public function refresh(Request $request)
   {
        
   }

   public function register(Request $request)
   {
       $validator = Validator::make($request->all(),
       [
           'USERNAME'=>'required',
           'EMAIL'=>'required|email',
           'PASSWORD'=>'required',
       ]);
       if($validator->fails())
       {
           return response()->json([
               'error'=>$validator->errors()
           ],401);
       }
       $input = $request->all();
       $input['PASSWORD']=Hash::make($input['PASSWORD']);
       $user = Client::create($input);
       $user = Client::where('USERNAME',$input['USERNAME'])->where('PASSWORD',$input['PASSWORD'])->first();
       $token=TokenClient::where('ID_CLIENT',$user->ID)->first();
       if($token==null)
       {
        $token = new TokenClient;
        $token['ID_CLIENT']=$user['ID'];
        $token['PRIVILEDGE']='0';
        $token['TOKEN']=Hash::make(rand());
       }
       $token['START']=date('Y-m-d h:i:s', time());
       $token['FINISH']=date('Y-m-d h:i:s', time()+60*60*8);
       $token->save();
       $success['USERNAME']=$user->USERNAME;
       return response()->json([
           'success'=>$token->TOKEN
       ],$this->successStatus)->withCookie(cookie('token',$token['TOKEN'],60))->withCookie(cookie('type','client',60));
   }

   /**
     * getDetails.
     *
     * @param  Request  $request
     * @return Response
     */
   public function getDetails(Request $request)
   {
       //$token = Cookie::get('token');
       $token = $request->token;
       $tc = TokenClient::where('TOKEN',$token)->WHERE('FINISH',TokenClient::max('FINISH'))->first();
       if($tc==null)
        return response()->json(['failiure'=>'Invalid token'],405);
       $user = Client::find($tc['ID_CLIENT']);
       if($user==null)
        {
         return response()->json(['failiure'=>'Token does not reference anyone. This should not happen ever.'],405);
        }
       return response()->json(['success'=>'Details in filed user','user'=>$user],
        $this->successStatus);
   }

   public function getDetailsEmployee(Request $request)
   {
       $token = $request->token;
       //dd($token);
       $tc = TokenEmployee::where('TOKEN',$token)->WHERE('FINISH',TokenEmployee::max('FINISH'))->first();
       if($tc==null)
        return response()->json(['failiure'=>'Invalid token'],405);
       $user = Employee::find($tc['ID_EMPLOYEE']);
       if($user==null)
        {
         return response()->json(['failiure'=>'Token does not reference anyone. This should not happen ever.'],401);
        }
       return response()->json(['success'=>'Details in filed user','user'=>$user],
        $this->successStatus);
   }

    public function loginEmployee()
    {
        $request['USERNAME']=request('USERNAME');
        $request['PASSWORD']=stripslashes(request('PASSWORD'));
        $u = Employee::where('USERNAME',$request['USERNAME'])->first();
        if($u==null)
            return response()->json(['error'=>'Unauthorised: username does not exist'],401);
        $password = $u['PASSWORD'];
        if(Hash::check($request['PASSWORD'],$password))
        {
            $user = Employee::where('USERNAME',$request['USERNAME'])->where('PASSWORD',$password)->first();
            $token = TokenEmployee::where('ID_EMPLOYEE',$user->ID)->where('FINISH',TokenEmployee::where('ID_EMPLOYEE',$user->ID)->max('FINISH'))->first();
            if($token==null)
                return response()->json(['failiure'=>'No token found!'],400);
            $timestamp = strtotime($token->FINISH);
            if($token==null || $timestamp<time())
            {
                $token = new TokenEmployee;
                $token->TOKEN=Hash::make(rand());
                $token->ID_EMPLOYEE = $user->ID;
                $token->PRIVILEDGE = 0;
                $token->START = date('Y-m-d h:i:s', time());
                $token->FINISH=date('Y-m-d h:i:s', time()+60*60*8);
                $token->save();
            }
            else
            {
                $token->FINISH = date('Y-m-d h:i:s', time()+60*60*8);
                $token->save();
            }
            return response()->json(['success'=>'You have been logged in.','token'=>$token->TOKEN],200)->withCookie(cookie('token',$token->TOKEN),60)->withCookie(cookie('type','employee',60));
        }
        else
            return response()->json(['error'=>'Unauthorised: wrong password'],401);
   }

   public function registerEmployee(Request $request)
   {
       $validator = Validator::make($request->all(),
       [
           'USERNAME'=>'required',
           'PASSWORD'=>'required',
       ]);
       if($validator->fails())
       {
           return response()->json([
               'error'=>$validator->errors()
           ],401);
       }
       $input = $request->all();
       $input['PASSWORD']=Hash::make($input['PASSWORD']);
       $user = Employee::create($input);
       $user = Employee::where('USERNAME',$input['USERNAME'])->where('PASSWORD',$input['PASSWORD'])->first();
       $token=TokenEmployee::where('ID_EMPLOYEE',$user->ID)->first();
       if($token==null)
       {
        $token = new TokenEmployee;
        $token['ID_EMPLOYEE']=$user['ID'];
        $token['PRIVILEDGE']='0';
        $token['TOKEN']=Hash::make(rand());
       }
       $token['START']=date('Y-m-d h:i:s', time());
       $token['FINISH']=date('Y-m-d h:i:s', time()+60*60*8);
       $token->save();
       $success['USERNAME']=$user->USERNAME;
       return response()->json([
           'success'=>$token,'token'=>$token->TOKEN
       ],$this->successStatus)->withCookie(cookie('token',$token['TOKEN'],60))->withCookie(cookie('type','employee',60));
   }
}
