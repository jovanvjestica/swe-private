<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Hotel as Hotel;


class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Hotel::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$this->lookUp('employee','admin'))
            return response()->json(['failiure'=>'Not authorized'],401);
        $h = new Hotel;
        $h->NAME = $request->NAME;
        $h->LOCATION = $request->LOCATION;
        $h->EMAIL = $request->EMAIL;
        $h->save();
        return response()->json(['success'=>'new hotel added!'],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$this->lookUp('employee','admin'))
            return response()->json(['failiure'=>'Not authorized'],401);
        $h = Hotel::find($id);
        $h->NAME = $request->NAME;
        $h->LOCATION = $request->LOCATION;
        $h->EMAIL = $request->EMAIL;
        $h->save();
        return response()->json(['success'=>'hotel updated!'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$this->lookUp('employee','admin'))
            return response()->json(['failiure'=>'Not authorized'],401);
        $Hotel->destroy($id);
        return response()->json(['success'=>'hotel ravaged!'],200);
    }

    public function getRooms($id)
    {
        return Hotel::find($id)->rooms();
    }

    public function getEmployees($id)
    {
        return Hotel::find($id)->employees();
    }
}
