<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event as Event;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Event::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       /* $e = new Event;
       // $e->ID= = $request->ID;
        $e->NAME = $request->NAME;
        $e->START = $request->START;
        $e->CAPACITY = $request->CAPACITY;
        $e->OPIS = $request->OPIS;
        $e->PRICE = $request->PRICE;*/
        
        if(!$this->lookUp('employee','manager'))
            return response()->json(['failiure'=>'Not authorized!'],401);
        Event::create($request->all());
        return response()->json(['success'=>'You did it!'],200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$e = Event::find($id);
       // $e->ID= = $request->ID;
        $e->NAME = $request->NAME;
        $e->START = $request->START;
        $e->CAPACITY = $request->CAPACITY;
        $e->OPIS = $request->OPIS;
        $e->PRICE = $request->PRICE;*/
        if(!$this->lookUp('employee','manager'))
            return response()->json(['failiure'=>'Not Authorized!'],401);
        Event::find($id)->update($request->all());
        return response()->json(['success'=>'Great success!'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$this->lookUp('employee','manager'))
            return response()->json(['failiure'=>'Not Authorized!'],401);
        $e = Event::find($id);
        if($e==null)
            return response()->json(['failiure'=>'Resource not found!'],400);
        Event::destroy($id);
        return response()->json(['success'=>'Great success!'],200);
    }

    public function getClients($id)
    {
        return Event::find($id)->clients;
    }
}
