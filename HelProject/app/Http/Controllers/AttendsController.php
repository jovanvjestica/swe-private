<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AttendsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$this->lookUp('employee','regular'))
            return response()->json(['failiure'=>'Not authorised'],401);
        return Attends::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$this->lookUp('client','registered'))
            return response()->json(['failiure'=>'Not authorised'],401);
        $token = $request->cookie('token');
        $tc = TokenClient::where('TOKEN',$token)->first();
        $user = Client::find($tc->ID_CLIENT);
        Attends::create($request->all());
        return response()->json(['success'=>'Added new attendance!'],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($this->lookUp('employee','admin'))
        {
            $a = Attends::find($id);
            $a->ID_CLIENT = $request['ID_CLIENT'];
            $a->ID_EVENT = $request['ID_EVENT'];
            $a->save();
            return response()->json(['success'=>'You have edited this attendance!'],200);
        }
        return response()->json(['failiure'=>'Access denied'],401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->getUser();
        if(!$this->lookUp('client','registered') || $user==null)
            return response()->json(['failiure'=>'Access denied'],401);
        $a = Attends::find($id);
        if($a->ID_CLIENT!=$user->ID)
            return response()->json(['failiure'=>'You can only delete your own attendances!'],401);
        Attends::destroy($id);
        return response()->json(['success'=>'Attendance cancelled!'],200);
    }
}
