<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Review as Review;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Review::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$this->lookUp('client','registered'))
            return response()->json(['failiure'=>'Unauthorised!'],401);
        $user = $this->getUser();
        if($user->ID!=$request['ID_CLIENT'])
            return response()->json(['failiure'=>'Cant post as someone else!'],400);
        Review::create($request->all());
        return response()->json(['success'=>'You did it fam!'],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$this->lookUp('client','registered'))
            return response()->json(['failiure'=>'Unauthorised!'],401);
        $user = $this->getUser();
        if($user->ID!=$request['ID_CLIENT'])
            return response()->json(['failiure'=>'Cant post as someone else!'],400);
        $r = Review::find($id);
        if($r==null)
            return response()->json(['failiure'=>'No such review!!'],400);
        $r->update($request->all());
        return response()->json(['success'=>'You did it fam!'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$this->lookUp('client','registered') || !$this->lookUp('employee','manager'))
            return response()->json(['failiure'=>'Unauthorised!'],401);
        $r = Review::find($id);
        $user = $this->getUser();
        if($r->ID_CLIENT==$user->ID || $this->lookUp('employee','manager'))
        Review::destroy($id);
    }
}
