<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Service as Service;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Service::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $s = new Service;
        $s->PRICE = $request->PRICE;
        $s->VALID_FROM = $request->VALID_FROM;
        $s->VALID_TO = $request->VALID_TO;
        $s->ID_HOTEL = $request->ID_HOTEL;
        $s->TYPE = $request->TYPE;
        $s->ROOM_TYPE = $request->ROOM_TYPE;
        $s->RNR_TYPE = $request->RNR_TYPE;
        $s->LENGTH = $request->LENGTH;
        $s->START_TIME = $request->START_TIME;
        $s->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $s = Service::find($id);
        $s->PRICE = $request->PRICE;
        $s->VALID_FROM = $request->VALID_FROM;
        $s->VALID_TO = $request->VALID_TO;
        $s->ID_HOTEL = $request->ID_HOTEL;
        $s->TYPE = $request->TYPE;
        $s->ROOM_TYPE = $request->ROOM_TYPE;
        $s->RNR_TYPE = $request->RNR_TYPE;
        $s->LENGTH = $request->LENGTH;
        $s->START_TIME = $request->START_TIME;
        $s->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Service::destory($id);
    }

    public function getFoods($id)
    {
        return Service::find($id)->foods;
    }

    public function getHotel($id)
    {
        return Service::find($id)->hotel;
    }
}
