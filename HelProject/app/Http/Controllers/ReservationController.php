<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Reservation as Reservation;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->lookUp('client','registered'))
        {
            $user = $this->getUser();
            $r = Reservation::where('ID_CLIENT',$user->ID)->first();
            return $r;
        }
        else if(!$this->lookUp('employee','regular'))
            return response()->json(['failiure'=>'Not authorized'],401);
        return Reservation::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$r = new Reservation;
        $r->START = $request->START;
        $r->FINISH = $request->FINISH;
        $r->RESERVATION_DATE = $request->RESERVATION_DATE;
        $r->ID_ROOM = $request->ID_ROOM;
        $r->save();*/
        Reservation::create($request->all());
        return response()->json(['success'=>'Successful reservation!'],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$r = Reservation::find($id);
        $r->START = $request->START;
        $r->FINISH = $request->FINISH;
        $r->RESERVATION_DATE = $request->RESERVATION_DATE;
        $r->ID_ROOM = $request->ID_ROOM;
        $r->save();*/
        if($this->lookUp('client','unregistered'))
        {
            $user = $this->getUser();
            $r = Reservation::find($id);
            if($r->CLIENT_ID!=$user->ID)
                return response()->json(['failiure'=>'Not authorized!'],401);
            $r->update($reservation->all());
            return response()->json(['success'=>'You did it again you!'],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$this->lookUp('client','unregistered')|| !$this->lookUp('employee','manager'))
            return response()->json(['failiure'=>'Not authorized!'],401);
        $user = $this->getUser();
        $r = Reservation::find($id);
        if($r==null)
            return response()->json(['failiure'=>'No such reservation!'],400);
        if($this->lookUp('employee','manager') || $user->ID_RESERVATION==$id)
            Reservation::destroy($id);
        return response()->json(['success'=>'Reservation cancelled!',200]);
    }

    public function getClient($id)
    {
        return Reservation::find($id)->client;
    }

    public function getRoom($id)
    {
        return Reservation::find($id)->room;
    }
}
