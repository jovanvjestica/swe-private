<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Client as Client;
use App\TokenClient as TokenClient;
use Illuminate\Support\Facades\Hash as Hash;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$this->lookUp('employee','regular'))
            return response()->json(['failiure'=>'Not authorised'],401);
        return Client::all();
    }

    public function current(Request $request)
    {
        $token = $request->cookie('token');
        if($token==null)
            return response()->json(['failiure'=>'No token set in cookies. Please log in to obtain your valid cookie.'],401);
        $tc = TokenClient::where('TOKEN',$token)->first();
        if($tc==null)
            return response()->json(['failiure'=>'Token not associated with any user.'],401);
        $user = Client::find($tc->ID_CLIENT);
        return $user;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['REGISTERED']=0;
        $data['PASSWORD']=Hash::make($request['PASSWORD']);
        $c = Client::create($data);
        $c->save();
        return response()->json(['success'=>'New client added'],200);
    }

    public function registerClient(Request $request, $id)
    {
        if(!$this->lookUp('employee','regular'))
            return response()->json(['failiure'=>'Not authorised'],401);
        $user = Client::find($id);
        $user['REGISTERED']=1;
        return response()->json(['success'=>'Client registered and can now log in!'],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$this->lookUp('employee','regular'))
            return response()->json(['failiure'=>'Not authorised'],401);
        $test = Client::find($id);
        $request->ID = $id;
        $request['PASSWORD']=Hash::make($request['PASSWORD']);
        if($test==null)
            return response()->json(['failiure'=>'No such client'],400);
        $c = Client::find($id)->update($request->all());
        return response()->json(['success'=>'Client updated'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$this->lookUp('employee','regular'))
            return response()->json(['failiure'=>'Not authorised'],401);
        $c = Client::find($id);
        if($c==null)
            return response()->json(['failiure'=>'No such client!'],400);
        Client::destroy($id);
        return response()->json(['failiure'=>'Client succesfully destroyed!'],200);
    }

    public function getEvents($id)
    {
        $c = Client::find($id);
        return $c->events;
    }

    public function getReservation($id)
    {
        return Client::find($id)->reservation;
    }
}
