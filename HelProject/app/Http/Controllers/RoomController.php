<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Room as Room;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Room::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $r = new Room;
        $r->RESERVED = $request->RESERVED;
        $r->PHOTO = $request->PHOTO;
        $r->PRICE = $request->PRICE;
        $r->KITCHEN = $request->KITCHEN;
        $r->LAST_RENOVATED = $request->LAST_RENOVATED;
        $r->TYPE = $request->TYPE;
        $r->DESCRIPTION = $request->DESCRIPTION;
        $r->AC = $request->AC;
        $r->BALCONY = $request->BALCONY;
        $r->TYPE_EXCELCSIOR = $request->TYPE_EXCELCSIOR;
        $r->NUMBER_OF_ROOMS = $request->NUMBER_OF_ROOMS;
        $r->ID_HOTEL = $request->ID_HOTEL;
        $r->ROOM_NUMBER = $request->ROOM_NUMBER;
        $r->save();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $r = Room::find($id);
        $r->RESERVED = $request->RESERVED;
        $r->PHOTO = $request->PHOTO;
        $r->PRICE = $request->PRICE;
        $r->KITCHEN = $request->KITCHEN;
        $r->LAST_RENOVATED = $request->LAST_RENOVATED;
        $r->TYPE = $request->TYPE;
        $r->DESCRIPTION = $request->DESCRIPTION;
        $r->AC = $request->AC;
        $r->BALCONY = $request->BALCONY;
        $r->TYPE_EXCELCSIOR = $request->TYPE_EXCELCSIOR;
        $r->NUMBER_OF_ROOMS = $request->NUMBER_OF_ROOMS;
        $r->ID_HOTEL = $request->ID_HOTEL;
        $r->ROOM_NUMBER = $request->ROOM_NUMBER;
        $r->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Room::destroy($id);
    }

    public function findRooms(Request $request)
    {
        $tip = $request['TYPE'];
        $balcony = $request['BALCONY'];
        $rooms = Room::where('TYPE',$tip)->where('BALCONY',$balcony)->get();
        $startDate = $request['START_DATE'];
        $startTime = strtotime($startDate);
        $days = $request['DAYS']*60*60*24;
        $final=[];
        foreach($rooms as $room)
        {
            $bool = true;
            $rezervacije = Reservation::where('ID_ROOM',$room->ID)->get();
            foreach($rezervacije as $rezervacija)
            {
                $start = strtotime($rezervacija->START);
                $end = strtotime($rezervacija->FINISH);
                if(($start <= $startTime && $startTime <= $end) || ($start<= $startTime+$days && $startTime+$days<=$end))
                    $bool=false;
            }
            if($bool)
                $final[] = $room;
        }
        return $final;
    }

    public function getReservations($id)
    {
        return Room::find($id)->reservations;
    }

    public function getHotel($id)
    {
        return Room::find($id)->hotel;
    }
}
