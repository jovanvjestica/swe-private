<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Food as Food;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!$this->lookUp('employee','regular') || !$this->lookUp('client','registered'))
            return response()->json(['failiure'=>'Not Authorized!'],401);
        return Food::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$f = new Food;
        $f->NAME = $request->NAME;
        $f->PRICE = $request->PRICE;
        $f->SIZE = $request->SIZE;
        $f->PHOTO = $request->PHOTO;
        $f->SERVICE_ID = $request->SERVICE_ID;*/
        if(!$this->lookUp('employee','manager'))
            return response()->json(['failiure'=>'Not Authorized!'],401);
        Food::create($request->all());
        return response()->json(['success'=>'You added some food! Good on you!'],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$f = Food::find($id);
        $f->NAME = $request->NAME;
        $f->PRICE = $request->PRICE;
        $f->SIZE = $request->SIZE;
        $f->PHOTO = $request->PHOTO;
        $f->SERVICE_ID = $request->SERVICE_ID;*/
        if(!$this->lookUp('employee','manager'))
            return response()->json(['failiure'=>'Not Authorized!'],401);
        Food::find($id)->update($request->all());
        return response()->json(['success'=>'You updated some food! Good on you!'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$this->lookUp('employee','manager'))
            return response()->json(['failiure'=>'Not Authorized!'],401);
        $f = Food::find($id);
        if($f==null)
            return response()->json(['failiure'=>'Could not find the food!!'],400);
        Food::destroy($id);
        return response()->json(['success'=>'Back to ethiopia!!'],200);
    }

    public function getService($id)
    {
        return Food::find($id)->service;
    }
}
