<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "reservation";
    protected $primaryKey= "ID";
    protected $guarded = ['ID'];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function client()
    {
        return $this->belongsTo('App\Client','ID_RESERVATION','ID');
    }

    public function room()
    {
        return $this->belongsTo('App\Room','ID_ROOM','ID');
    }
}
