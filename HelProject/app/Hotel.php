<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "hotel";
    protected $primaryKey= "ID";
    protected $guarded = ['ID'];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false; 

    public function employees()
    {
        return $this->hasMany('App\Employee','ID_HOTEL','ID');
    }

    public function rooms()
    {
        return $this->hasMany('App\Room','ID_HOTEL','ID');
    }

    public function services()
    {
        return $this->hasMany('App\Service','ID_HOTEL','ID');
    }
}
