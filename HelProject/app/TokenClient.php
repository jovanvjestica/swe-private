<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TokenClient extends Model
{
    protected $table = "tokenclient";
    protected $primaryKey= "ID";
    public $timestamps = false;
    protected $guarded = ['ID'];
}
