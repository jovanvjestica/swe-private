<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TokenEmployee extends Model
{
    protected $table = "tokenemployee";
    protected $primaryKey= "ID";
    public $timestamps = false;
    protected $guarded = ['ID'];
}
