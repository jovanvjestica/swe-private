<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "event";
    protected $primaryKey= "ID";
    protected $guarded = ['ID'];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function clients()
    {
        return $this->belongsToMany('App\Client','attends','ID_EVENT','ID_CLIENT');
    }
}
