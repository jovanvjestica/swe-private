<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "room";
    protected $primaryKey= "ID";
    protected $guarded = ['ID'];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function reservations()
    {
        return $this->hasMany('App\Reservation','ID_ROOM','ID');
    }

    public function hotel()
    {
        return $this->belongTo('App\Hotel','ID_HOTEL','ID');
    }
}
